import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
// import { environment } from 'src/environments/environment';
import { AppConfigService } from './AppConfigService';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiUrl: String;
  private headers: HttpHeaders;
  protected API_URL = AppConfigService.settings.API_URL;

  constructor(private http: HttpClient) {
    // API URL
    this.apiUrl = this.API_URL.link1;
    this.headers = new HttpHeaders().set('Content-Type', 'application/json');
  }

  getTableNames(connectionString: string): Observable<any> {
    const params = new HttpParams().set('connectionString', connectionString);
    return this.http.get(`${this.apiUrl}/api/tables`, { params: params, headers: this.headers });
  }

  getTable(connectionString: string, tableName: string): Observable<any> {
    const params = new HttpParams().set('connectionString', connectionString).set('tableName', tableName);
    return this.http.get(`${this.apiUrl}/api/table`, { params: params, headers: this.headers });
  }

  editTableRow(connectionString: string, tableName: string, newRowValues: any, oldRowValues: any): Observable<any> {
    const params = new HttpParams().set('connectionString', connectionString).set('tableName', tableName);
    return this.http.put(`${this.apiUrl}/api/table`, { newRowValues: newRowValues, oldRowValues: oldRowValues }, { params: params, headers: this.headers });
  }

  addTableRow(connectionString: string, tableName: string, newRowValues: any): Observable<any> {
    const params = new HttpParams().set('connectionString', connectionString).set('tableName', tableName);
    return this.http.post(`${this.apiUrl}/api/table`, newRowValues, { params: params, headers: this.headers });
  }

}


