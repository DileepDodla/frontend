import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ApiService } from './api.service';
import { AppConfigService } from "./AppConfigService";
// import { environment } from 'src/environments/environment';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  protected dbSettings = AppConfigService.settings.dbSettings;
  getConnectionStringForm: FormGroup;

  selectedConnectionString: string;

  allConnectionStrings: string[];
  maskedConnectionStrings: string[];

  submitted: boolean;

  tableNames: string[];
  tableHeaders: string[];
  tableRows: string[][];

  identityColumnName: string;
  selectedTableName: string;
  selectedEditRowTableData: any;

  updateTableForm: FormGroup;
  isEditTableRow: boolean;

  filterText: string;

  constructor(private fb: FormBuilder, private snackBar: MatSnackBar, private apiService: ApiService) {

    this.selectedConnectionString = "";

    this.allConnectionStrings = Object.values(this.dbSettings);
    this.maskedConnectionStrings = this.allConnectionStrings.map((connStr: string) => this.getMaskedConnectionString(connStr));

    this.getConnectionStringForm = this.fb.group({
      connectionStringId: [""]
    });

    this.submitted = false;

    this.tableNames = [];
    this.tableHeaders = [];
    this.tableRows = [];

    this.identityColumnName = "";
    this.selectedTableName = "";
    this.selectedEditRowTableData = {};

    this.updateTableForm = this.fb.group({});
    this.isEditTableRow = false;

    this.filterText = "";
  }

  getMaskedConnectionString(connectionString: string): string {
    connectionString = connectionString.replace(/User Id=[^;]*/gi, 'User Id=********');
    connectionString = connectionString.replace(/Password=[^;]*/gi, 'Password=********');
    return connectionString;
  }

  onLoad(): void {
    if (this.getConnectionStringForm.valid) {
      this.tableNames = [];
      this.tableHeaders = [];
      this.tableRows = [];
      this.selectedConnectionString = this.allConnectionStrings[this.getConnectionStringForm.value.connectionStringId];
      this.apiService.getTableNames(this.selectedConnectionString).subscribe(
        data => {
          this.tableNames = JSON.parse(data).sort();
          this.snackBar.open("✓ Success (Tables have been loaded)", "", { duration: 3000, panelClass: ['bg-success', 'text-light'] })
        },
        (err: HttpErrorResponse) => {
          this.snackBar.open((err.status == 0) ? "Cannot reach the server! Either the internet connection is down or the server might not be running." : err.error, "", { duration: 10000, panelClass: ['bg-danger', 'text-light'] });
          console.error(err);
        }
      );
      this.submitted = true;
    }
  }

  onTableNameSelected(tableName: string, index: number): void {
    // Highlight the selected table name
    document.querySelectorAll("#tableNames tbody tr").forEach(
      (element, key) => { (key == index) ? element.classList.add("table-active") : element.classList.remove("table-active") }
    );

    this.populateData(tableName);
  }

  populateData(tableName: string, showSnackBar: boolean = true): void {
    this.selectedTableName = tableName;

    this.apiService.getTable(this.selectedConnectionString, tableName).subscribe(
      data => {
        let responseData = JSON.parse(data);
        this.identityColumnName = responseData["identityColumnName"];
        this.tableHeaders = responseData["tableData"][0];
        this.tableRows = responseData["tableData"].slice(1);
        this.buildUpdateTableForm();
        if (showSnackBar)
          this.snackBar.open(`✓ Success (${this.selectedTableName} data has been populated)`, "", { duration: 3000, panelClass: ['bg-success', 'text-light'] })
      },
      (err: HttpErrorResponse) => {
        this.snackBar.open((err.status == 0) ? "Cannot reach the server! Either the internet connection is down or the server might not be running." : err.error, "", { duration: 8000, panelClass: ['bg-danger', 'text-light'] });
        console.log(err);
      }
    );

  }

  onEditRow(rowValues: any): void {
    this.isEditTableRow = true;
    this.updateTableForm.reset();
    this.selectedEditRowTableData = {};
    Object.assign(this.selectedEditRowTableData, ...this.tableHeaders.map((k, i) => ({ [k]: rowValues[i] })));
    this.updateTableForm.setValue(this.selectedEditRowTableData);

    if (this.selectedEditRowTableData.hasOwnProperty(this.identityColumnName)) {
      let val = this.selectedEditRowTableData[this.identityColumnName];
      this.selectedEditRowTableData = {};
      this.selectedEditRowTableData[this.identityColumnName] = val;
    };
  }

  onAddNewRow(): void {
    this.isEditTableRow = false;
    this.updateTableForm.reset();
  }

  buildUpdateTableForm(): void {
    this.updateTableForm = new FormGroup({});
    for (let tableHeader of this.tableHeaders)
      this.updateTableForm.addControl(tableHeader, new FormControl(['']));
  }

  onModalFormSubmit(): void {
    if (this.updateTableForm.valid) {
      var formData = this.updateTableForm.value;
      if (this.identityColumnName?.length != 0) {
        delete formData[this.identityColumnName];
      }
      for (let key of Object.keys(formData)) {
        let val = formData[key];
        if (val == null || val.trim() == "")
          val = "";
        formData[key] = val.trim();
      }
      if (this.isEditTableRow) {
        this.apiService.editTableRow(this.selectedConnectionString, this.selectedTableName, formData, this.selectedEditRowTableData).subscribe(
          response => {
            this.populateData(this.selectedTableName, false);
            document.getElementById("closeUpdateTableFormModal")?.click();
            this.snackBar.open(JSON.parse(response).message, "", { duration: 3000, panelClass: ['bg-success', 'text-light'] })
          },
          (err: HttpErrorResponse) => {
            this.snackBar.open((err.status == 0) ? "Cannot reach the server! Either the internet connection is down or the server might not be running." : err.error, "", { duration: 8000, panelClass: ['bg-danger', 'text-light'] });
          }
        );
      }
      else {
        this.apiService.addTableRow(this.selectedConnectionString, this.selectedTableName, formData).subscribe(
          response => {
            this.populateData(this.selectedTableName, false);
            document.getElementById("closeUpdateTableFormModal")?.click();
            this.snackBar.open(JSON.parse(response).message, "", { duration: 3000, panelClass: ['bg-success', 'text-light'] })
          },
          (err: HttpErrorResponse) => {
            this.snackBar.open((err.status == 0) ? "Cannot reach the server! Either the internet connection is down or the server might not be running." : err.error, "", { duration: 8000, panelClass: ['bg-danger', 'text-light'] });
          }
        );
      }
    }
    else {
      this.updateTableForm.markAllAsTouched();
    }
  }

  sortData(sort: Sort) {
    const data = this.tableRows.slice();
    if (!sort.active || sort.direction === '') {
      this.tableRows = data;
      return;
    }

    this.tableRows = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      let ind = this.tableHeaders.indexOf(sort.active);
      return compare(isNaN(+a[ind]) ? a[ind] : +a[ind], isNaN(+b[ind]) ? b[ind] : +b[ind], isAsc);
    });
    this.snackBar.open(`Table sorted according to ${sort.active} in ${sort.direction} order`, "", { duration: 5000, panelClass: ['bg-primary', 'text-light'] })
  }
}


function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
