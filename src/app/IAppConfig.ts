
export interface IAppConfig {

  env: {
    appTitle: string;
  };

  API_URL: {
    link1: string;
    link2: string;
  };

  dbSettings: {
    DB_CONNECTION_01: string;
    DB_CONNECTION_02: string;
  };
}
